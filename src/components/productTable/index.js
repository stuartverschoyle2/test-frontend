import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'title', headerName: 'Title', width: 130 },
  { field: 'tags', headerName: 'Tags', width: 130 },
  { field: 'subscription', headerName: 'Subscription', width: 130 },
  { field: 'price', headerName: 'Price', width: 130 },
];


export default function DataTable({data}) {
  return (
    <div style={{ height: 400, width: '100%' }}>
      <DataGrid
        rows={data}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
        checkboxSelection
      />
    </div>
  );
}