
import axios from "axios";
import { useEffect, useState } from "react";
import Grid from '@mui/material/Grid';
import ProductTable from '../components/productTable';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import TextField from '@mui/material/TextField';

export default function Expenses() {
    const [products, setProducts] = useState([])
    const [search, setSearch] = useState("")
    const [subscribed, setChecked] = useState(false);
    const [pricematch, setPrice] = useState();

    const getProducts = async () => {
      try {
        const products = await axios.get(
          "http://localhost:3010/products"
        );
        setProducts(products.data);
      } catch (e) {
        console.log(e);
      }
      };
       
      
    useEffect(()=>{
        getProducts();
    }, [pricematch])

  
    const results = () => {
        const filters = []
    
        if (pricematch) {
          filters.push((product) => product.price <= pricematch)
        }
        if (subscribed) {
          filters.push((product) => product.subscription === true)
        }
        if (search) {
          filters.push((product) =>
            product.tags.some((tag) =>
              tag.toLowerCase().includes(search.toLowerCase())
            )
          )
        }
    
        return products.filter((product) => {
          return filters.reduce((acc, filter) => acc && filter(product), true)
        })
    }

    return (
      <main className="collection">
        <Grid container spacing={2}>
            <Grid item xs={12} md={4}>
                <h2>Tag Filters</h2>
                <TextField 
                    id="tags" 
                    label="Search tag" 
                    onChange={(e)=>{setSearch(e.target.value)}}  />
                <h2>Price Filter</h2>
                <TextField
                    id="price"
                    label="Price"
                    type="number"
                    onChange={e=>{setPrice(e.target.value)}} />                              
                <FormGroup>
                    <FormControlLabel control={<Checkbox checked={subscribed} onChange={e=>setChecked(e.target.checked)} />} label="Subscribed" />
                </FormGroup>                
            </Grid>
            <Grid item xs={12} md={8}>
                <h1>Product collection</h1>
                <ProductTable data={results()} />
            </Grid>
        </Grid>        
      </main>
    );
  }