import logo from './logo.svg';
import './App.scss';
import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';
 
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Button component={Link} to="/product-collection" variant="contained" color="primary">
          View Collection 
        </Button>        
      </header>
    </div>
  );
}

export default App;
