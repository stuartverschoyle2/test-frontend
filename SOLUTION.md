# SOLUTION

3 test cases

Example: user wants to filter tags
Given there are 5 tags
When a tag is entered
Then only tags that match will show

Example: user wants to filter tags & subscritions
Given there are 5 tags
When a tag is entered
And subscription is set to true
Then only tags that are subscribed will show

Example: user wants to filter tags & subscritions & price
Given there are 5 tags
And a subscription true false
And a price is set  
 And a tag is entered
Then only tags that are subscribed will show that are under that price

Installed MUI Library
Created a page called product collection
Seperate filters on the left hand side for tags, price and subscribed.

## Estimation

Estimated: 4 hours

Spent: 6 hours

## Solution

Comments on your solution

I used MUI as its a nice UI library to work with, but would be better to write completely from scratch given a longer period of time as it will be less heavy.
Using MUI was also a very quick way to get the data into a sortable data table
with pagination.

The filter functions that I wrote is easily expandable if another filter was put in place. The filters also populate after each input.

I would improve the UI by having the tag filter allow multiple tags,
the Price filter could be a slider. Having a visible log of your filters
which can be removed to update results would also be benificial to the end user.
An auto generate input box would aslo help the end user to make descisions which are available.
